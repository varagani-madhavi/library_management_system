package com.wipro.LibraryManagement.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.wipro.LibraryManagement.model.LibraryBooks;

@Repository
public interface LibraryBooksRepository extends JpaRepository<LibraryBooks, Integer> {

	public List<LibraryBooks> findBybookName(String bookName);

	public List<LibraryBooks> findByauthor(String author);

}
