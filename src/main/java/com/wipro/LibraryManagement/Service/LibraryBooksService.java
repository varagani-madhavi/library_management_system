package com.wipro.LibraryManagement.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.wipro.LibraryManagement.Repository.LibraryBooksRepository;
import com.wipro.LibraryManagement.model.LibraryBooks;

@Service
public class LibraryBooksService implements LibraryBooksServiceDao {
	@Autowired
	private LibraryBooksRepository booksRepository;

	// day1
	@Override
	public List<LibraryBooks> getAllBooks() {
		return booksRepository.findAll();
	}

	@Override
	public LibraryBooks getById(int bookID) {
		return booksRepository.findById(bookID).get();
	}

	@Override
	public List<LibraryBooks> getByName(String bookName) {
		return booksRepository.findBybookName(bookName);
	}

	@Override
	public List<LibraryBooks> getByAuthor(String author) {
		return booksRepository.findByauthor(author);
	}

	@Override
	public LibraryBooks getByLastAdded() {
		List<LibraryBooks> bookList = booksRepository.findAll();
		return bookList.get(bookList.size() - 1);
	}

	@Override
	public List<LibraryBooks> getBooksByAscendingOrder() {
		return booksRepository.findAll(Sort.by("bookName"));
	}

	// day2
	@Override
	public List<LibraryBooks> addLibraryBooks(List<LibraryBooks> books) {
		return booksRepository.saveAll(books);
	}

	@Override
	public void deleteById(int id) {
		booksRepository.deleteById(id);
	}

	@Override
	public void deleteAllBooks() {
		booksRepository.deleteAll();
	}

}
