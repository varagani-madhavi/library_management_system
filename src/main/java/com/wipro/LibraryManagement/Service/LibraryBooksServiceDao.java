package com.wipro.LibraryManagement.Service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.wipro.LibraryManagement.model.LibraryBooks;

@Service
public interface LibraryBooksServiceDao {

	// day1
	List<LibraryBooks> getAllBooks();

	LibraryBooks getById(int id);

	List<LibraryBooks> getByName(String bookName);

	List<LibraryBooks> getByAuthor(String author);

	LibraryBooks getByLastAdded();

	List<LibraryBooks> getBooksByAscendingOrder();

	// day2
	List<LibraryBooks> addLibraryBooks(List<LibraryBooks> books);

	public void deleteById(int id);

	public void deleteAllBooks();
}