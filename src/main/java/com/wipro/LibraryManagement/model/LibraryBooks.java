package com.wipro.LibraryManagement.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class LibraryBooks {
	@Id
	@GeneratedValue
	private int bookID;
	private String bookName;
	private String author;
	private String cost;
	private int count;
}
