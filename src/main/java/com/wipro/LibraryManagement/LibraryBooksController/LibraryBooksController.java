package com.wipro.LibraryManagement.LibraryBooksController;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.LibraryManagement.Service.LibraryBooksService;
import com.wipro.LibraryManagement.model.LibraryBooks;

@RestController
@RequestMapping("/api")
public class LibraryBooksController {

	// Day 3

	@Autowired
	private LibraryBooksService booksService;

	@GetMapping("/getBooks")
	public List<LibraryBooks> getAllBooks() {
		return booksService.getAllBooks();
	}

	@GetMapping("/getBook/{bookID}")
	public LibraryBooks getBookById(@PathVariable int bookID) {
		return booksService.getById(bookID);
	}

	@GetMapping("/getBooksByName/{bookName}")
	public List<LibraryBooks> getByName(@PathVariable String bookName) {
		return booksService.getByName(bookName);
	}

	@GetMapping("/getBooksByLastAdded")
	public LibraryBooks getByLastAdded() {
		return booksService.getByLastAdded();
	}

	@GetMapping("/getByAuthor/{author}")
	public List<LibraryBooks> getBookByAuthor(@PathVariable String author) {
		return booksService.getByAuthor(author);
	}

	@GetMapping("/getBooksByAscOrd")
	public List<LibraryBooks> getBooksByAscendingOrder() {
		return booksService.getBooksByAscendingOrder();
	}

	@PostMapping("/addBooks")
	public List<LibraryBooks> addBooks(@RequestBody List<LibraryBooks> books) {
		return booksService.addLibraryBooks(books);
	}

	@DeleteMapping("/deleteAllBooks")
	public ResponseEntity<String> deleteAllBooks() {
		booksService.deleteAllBooks();
		return new ResponseEntity<String>("records are deleted", HttpStatus.OK);
	}

	@DeleteMapping("/deleteById/{id}")
	public ResponseEntity<String> deleteById(@PathVariable int id) {
		booksService.deleteById(id);
		return new ResponseEntity<String>("record is deleted", HttpStatus.OK);
	}
}
